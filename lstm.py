import torch
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
from torch.autograd import Variable


class LSTM(nn.Module):
    def __init__(self) -> None:
        super(LSTM, self).__init__()

        self.word_embedding = nn.Embedding(60000, 128)

        self.lstm = nn.LSTM(128, 128, batch_first=True)
        self.dense = nn.Linear(128, 8)
        self.dpt = nn.Dropout(p=0.2)

    def forward(self, x):
        _x = self.word_embedding(x)
        bz = _x.shape[0]
        h_0 = Variable(torch.zeros(1, bz, 128))
        c_0 = Variable(torch.zeros(1, bz, 128))
        # h_0 = Variable(torch.zeros(1, bz, 128).cuda())
        # c_0 = Variable(torch.zeros(1, bz, 128).cuda())
        output, (final_hidden_state, final_cell_state) = self.lstm(
            _x, (h_0, c_0))
        _x = self.dpt(final_cell_state[-1])
        _x = self.dense(_x)
        return _x
