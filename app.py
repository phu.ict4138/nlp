import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QStackedWidget
from PyQt5.uic import loadUi
import pandas as pd
from underthesea import word_tokenize
from lstm import LSTM
from cnn_attention import CNNAttention
import torch
from dataset import Data
from torch.utils.data import DataLoader
import pickle


def process_sent(sent):
    x = sent.replace("\"", "")
    x = x.replace("?", "")
    x = x.replace(":", "")
    x = x.replace("!", "")
    x = x.replace("(", "")
    x = x.replace(")", "")
    x = x.replace("...", "")
    x = x.replace(" ._", " . ")
    x = x.replace(" >> ", " ")
    x = x.replace(" >>", " ")
    x = word_tokenize(x, format="text").lower()
    return x


cate = {
    0: "chinh-tri",
    1: "doi-song",
    2: "giao-duc",
    3: "kinh-doanh",
    4: "phap-luat",
    5: "suc-khoe",
    6: "the-gioi",
    7: "the-thao",
}


class App(QMainWindow):
    def __init__(self):
        super(App, self).__init__()
        loadUi("main.ui", self)
        self.button_predict.clicked.connect(self.predict)
        self.load_model()

    def load_model(self):
        model = LSTM()
        checkpoint = torch.load("./checkpoint/ckpt_lstm.pth")
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        self.model = model

        attention = CNNAttention()
        checkpoint = torch.load("./checkpoint/ckpt_cnn_attention.pth")
        attention.load_state_dict(checkpoint['model_state_dict'])
        attention.eval()
        self.attention = attention

        with open('svm_vectorizer.pk', 'rb') as fp:
            vec_svm = pickle.load(fp)
        with open('svm_svd.p', 'rb') as fp:
            svd_svm = pickle.load(fp)
        with open('svm_model.p', 'rb') as fp:
            svm_model = pickle.load(fp)
        self.svm_vec = vec_svm
        self.svm_svd = svd_svm
        self.svm_model = svm_model

        with open('random_vectorizer.pk', 'rb') as fp:
            vec_random = pickle.load(fp)
        with open('random_svd.p', 'rb') as fp:
            svd_random = pickle.load(fp)
        with open('random_model.p', 'rb') as fp:
            random_model = pickle.load(fp)
        self.random_vec = vec_random
        self.random_svd = svd_random
        self.random_model = random_model

        with open('bayes_vectorizer.pk', 'rb') as fp:
            vec_bayes = pickle.load(fp)
        with open('bayes_model.p', 'rb') as fp:
            bayes_model = pickle.load(fp)
        self.bayes_vec = vec_bayes
        self.bayes_model = bayes_model

        with open('logic_vectorizer.pk', 'rb') as fp:
            vec_logic = pickle.load(fp)
        with open('logic_model.p', 'rb') as fp:
            logic_model = pickle.load(fp)
        self.logic_vec = vec_logic
        self.logic_model = logic_model
        return

    def predict(self):
        sent = self.content.toPlainText()
        sent = process_sent(sent)
        k = [sent]
        l = [0]
        test = Data(k, l)
        test = DataLoader(test, batch_size=1)
        i = iter(test)
        o = next(i)
        pred = self.model(o["text"])
        predicted = torch.max(pred, 1)[1].tolist()
        text_lstm = cate[predicted[0]]
        self.lstm.setText(text_lstm)
        self.process.setText(sent)

        pred = self.attention(o["text"])
        predicted = torch.max(pred, 1)[1].tolist()
        text_attention = cate[predicted[0]]
        self.attention_.setText(text_attention)

        m = self.svm_vec.transform(k)
        n = self.svm_svd.transform(m)
        j = self.svm_model.predict(n)
        text_svm = cate[j[0]]
        self.svm.setText(text_svm)

        m = self.random_vec.transform(k)
        n = self.random_svd.transform(m)
        j = self.random_model.predict(n)
        text_random = cate[j[0]]
        self.rand.setText(text_random)

        m = self.bayes_vec.transform(k)
        j = self.bayes_model.predict(m)
        text_bayes = cate[j[0]]
        self.bayes.setText(text_bayes)

        m = self.logic_vec.transform(k)
        j = self.logic_model.predict(m)
        text_logic = cate[j[0]]
        self.logic.setText(text_logic)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setApplicationDisplayName("Một con vịt")
    widget = QStackedWidget()
    main = App()
    widget.addWidget(main)
    widget.setFixedWidth(1700)
    widget.setFixedHeight(900)
    widget.show()
    try:
        sys.exit(app.exec_())
    except:
        print("Closing")
