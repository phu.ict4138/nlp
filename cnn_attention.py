from layer import Attention
import torch.nn as nn


class CNNAttention(nn.Module):
    def __init__(self) -> None:
        super(CNNAttention, self).__init__()

        self.word_embedding = nn.Embedding(60000, 128)

        self.cnn = nn.Conv2d(
            in_channels=1,
            out_channels=400,
            kernel_size=(3, 128),
            padding=(1, 0)
        )
        self.relu_cnn = nn.ReLU()
        self.attention = Attention(400, 200)
        self.dense = nn.Linear(400, 8)
        self.dpt = nn.Dropout(p=0.2)

    def forward(self, x):
        _x = self.word_embedding(x)
        _x = _x.unsqueeze(1)
        _x = self.cnn(_x)
        _x = self.relu_cnn(_x)
        _x = _x.squeeze(-1)
        _x = _x.transpose(2, 1)
        _x = self.attention(_x)
        _x = self.dpt(_x)
        _x = self.dense(_x)
        return _x
