import torch
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
import pandas as pd


class Data(Dataset):
    def __init__(self, data, labels, number_class=8, max_length=150):
        super(Data, self).__init__()
        self.word_dict = dict(pd.read_table(
            "./word2index_60000.csv").values.tolist())
        self.data = data
        self.labels = labels
        self.number_class = 8
        self.max_length = max_length
        self.X = []
        self.Y = []
        self.parcing_data()
        print(len(self.X))
        print(len(self.Y))

    def parcing_data(self):
        y = [0] * self.number_class
        for i in tqdm(range(len(self.data)), desc="Parcing data ..."):
            j = self.data[i].lower().split(" ")
            n = len(j)
            if n > self.max_length:
                j = j[:self.max_length]
                _x = [0] * self.max_length
                for m, k in enumerate(j):
                    if k in self.word_dict:
                        _x[m] = self.word_dict[k]
                self.X.append(_x)
            else:
                _x = [0]*n + [0]*(self.max_length - n)
                for m, k in enumerate(j):
                    if k in self.word_dict:
                        _x[m] = self.word_dict[k]
                self.X.append(_x)
            _y = y.copy()
            _y[self.labels[i]] = 1
            self.Y.append(_y)

    def __len__(self):
        return len(self.X)

    def __getitem__(self, index):
        item = {}
        X = self.X[index]
        Y = self.Y[index]
        item["text"] = torch.tensor(X)
        item["label"] = torch.tensor(Y)
        return item
